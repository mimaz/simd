#pragma once

__SIMD_PURE_FUNC simd64i simd128i_lo (simd128i a)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_extract_epi64 (a, 0);
#else
    return a.l;
#endif
}

__SIMD_PURE_FUNC simd64i simd128i_hi (simd128i a)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_extract_epi64 (a, 1);
#else
    return a.h;
#endif
}

__SIMD_PURE_FUNC uint8_t simd128i_get8 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    switch (n) {
    case 0:
	return _mm_extract_epi8 (a, 0);

    case 1:
	return _mm_extract_epi8 (a, 1);

    case 2:
	return _mm_extract_epi8 (a, 2);

    case 3:
	return _mm_extract_epi8 (a, 3);

    case 4:
	return _mm_extract_epi8 (a, 4);

    case 5:
	return _mm_extract_epi8 (a, 5);

    case 6:
	return _mm_extract_epi8 (a, 6);

    case 7:
	return _mm_extract_epi8 (a, 7);

    default:
	__builtin_unreachable ();
    }
#else
    if (n < 8) {
	return simd64i_get8 (simd128i_lo (a), n);
    }

    return simd64i_get8 (simd128i_hi (a), n - 8);
#endif
}

__SIMD_PURE_FUNC uint16_t simd128i_get16 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_extract_epi16 (a, n);
#else
    if (n < 4) {
	return simd64i_get16 (simd128i_lo (a), n);
    }

    return simd64i_get16 (simd128i_hi (a), n - 4);
#endif
}

__SIMD_PURE_FUNC uint32_t simd128i_get32 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    switch (n) {
    case 0:
	return _mm_extract_epi32 (a, 0);

    case 1:
	return _mm_extract_epi32 (a, 1);

    case 2:
	return _mm_extract_epi32 (a, 2);

    case 3:
	return _mm_extract_epi32 (a, 3);

    default:
	__builtin_unreachable ();
    }
#else
    if (n < 2) {
	return simd64i_get32 (simd128i_lo (a), n);
    }

    return simd64i_get32 (simd128i_hi (a), n - 2);
#endif
}

__SIMD_PURE_FUNC uint64_t simd128i_get64 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    switch (n) {
    case 0:
	return _mm_extract_epi64 (a, 0);

    case 1:
	return _mm_extract_epi64 (a, 1);

    default:
	__builtin_unreachable ();
    }
#else
    if (n < 1) {
	return simd64i_get64 (simd128i_lo (a));
    }

    return simd64i_get64 (simd128i_hi (a));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_set_lohi (simd64i l, simd64i h)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set_epi64x (simd64i_get64 (h), simd64i_get64 (l));
#else
    return (simd128i) { l, h };
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_set8 (uint8_t v0, uint8_t v1, uint8_t v2, uint8_t v3,
					 uint8_t v4, uint8_t v5, uint8_t v6, uint8_t v7,
					 uint8_t v8, uint8_t v9, uint8_t v10, uint8_t v11,
					 uint8_t v12, uint8_t v13, uint8_t v14, uint8_t v15)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set_epi8 (v15, v14, v13, v12,
			 v11, v10, v9, v8,
			 v7, v6, v5, v4,
			 v3, v2, v1, v0);
#else
    union {
	uint8_t a[16];
	simd128i v;
    } u = { .a = { v0, v1, v2, v3, v4, v5, v6, v7,
		   v8, v9, v10, v11, v12, v13, v14, v15 } };

    return u.v;
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_set16 (uint16_t v0, uint16_t v1, uint16_t v2, uint16_t v3,
					  uint16_t v4, uint16_t v5, uint16_t v6, uint16_t v7)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set_epi16 (v7, v6, v5, v4,
			  v3, v2, v1, v0);
#else
    union {
	uint16_t a[8];
	simd128i v;
    } u = { .a = { v0, v1, v2, v3, v4, v5, v6, v7 } };

    return u.v;
#endif
}


__SIMD_PURE_FUNC simd128i simd128i_set32 (uint32_t v0, uint32_t v1, uint32_t v2, uint32_t v3)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set_epi32 (v3, v2, v1, v0);
#else
    union {
	uint32_t a[4];
	simd128i v;
    } u = { .a = { v0, v1, v2, v3 } };

    return u.v;
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_dup8 (uint8_t v)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set1_epi8 (v);
#else
    return simd128i_set8 (v, v, v, v, v, v, v, v, v, v, v, v, v, v, v, v);
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_dup16 (uint16_t v)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set1_epi16 (v);
#else
    return simd128i_set16 (v, v, v, v, v, v, v, v);
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_dup32 (uint32_t v)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set1_epi32 (v);
#else
    return simd128i_set32 (v, v, v, v);
#endif
}

__SIMD_CONST_FUNC simd128i simd128i_load (const void *p)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_loadu_si128 (p);
#else
    return *(const simd128i *) p;
#endif
}

__SIMD_FUNC void simd128i_store (simd128i a, void *p)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_storeu_si128 (p, a);
#else
    *(simd128i *) p = a;
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_add8 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_add_epi8 (a, b);
#else
    return simd128i_set_lohi (simd64i_add8 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_add8 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_add16 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_add_epi16 (a, b);
#else
    return simd128i_set_lohi (simd64i_add16 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_add16 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_add32 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_add_epi32 (a, b);
#else
    return simd128i_set_lohi (simd64i_add32 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_add32 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_sub8 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_sub_epi8 (a, b);
#else
    return simd128i_set_lohi (simd64i_sub8 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_sub8 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_sub16 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_sub_epi16 (a, b);
#else
    return simd128i_set_lohi (simd64i_sub16 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_sub16 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_sub32 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_sub_epi32 (a, b);
#else
    return simd128i_set_lohi (simd64i_sub32 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_sub32 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_mul32 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_mullo_epi32 (a, b);
#else
    return simd128i_set_lohi (simd64i_mul32 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_mul32 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_div32 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_cvtps_epi32 (_mm_div_ps (_mm_cvtepi32_ps (a), _mm_cvtepi32_ps (b)));
#else
    return simd128i_set_lohi (simd64i_div32 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_div32 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_or (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_or_si128 (a, b);
#else
    return simd128i_set_lohi (simd64i_or (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_or (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_and (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_and_si128 (a, b);
#else
    return simd128i_set_lohi (simd64i_and (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_and (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_andnot (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_andnot_si128 (a, b);
#else
    return simd128i_set_lohi (simd64i_andnot (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_andnot (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_xor (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_xor_si128 (a, b);
#else
    return simd128i_set_lohi (simd64i_xor (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_xor (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_neg8 (simd128i a)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_sign_epi8 (a, _mm_set1_epi8 (-1));
#else
    return simd128i_set_lohi (simd64i_neg8 (simd128i_lo (a)),
			      simd64i_neg8 (simd128i_hi (a)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_sign8 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_sign_epi8 (a, b);
#else
    return simd128i_set_lohi (simd64i_sign8 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_sign8 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_blend8 (simd128i a, simd128i b, simd128i m)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_blendv_epi8 (a, b, m);
#else
    return simd128i_set_lohi (simd64i_blend8 (simd128i_lo (a), simd128i_lo (b),
					      simd128i_lo (m)),
			      simd64i_blend8 (simd128i_hi (a), simd128i_hi (b),
					      simd128i_hi (m)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shl8 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_and_si128 (_mm_slli_epi64 (a, n), _mm_set1_epi8 (0xff << n));
#else
    return simd128i_set_lohi (simd64i_shl8 (simd128i_lo (a), n),
			      simd64i_shl8 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shl16 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_slli_epi16 (a, n);
#else
    return simd128i_set_lohi (simd64i_shl16 (simd128i_lo (a), n),
			      simd64i_shl16 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shl32 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_slli_epi32 (a, n);
#else
    return simd128i_set_lohi (simd64i_shl32 (simd128i_lo (a), n),
			      simd64i_shl32 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shl64 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_slli_epi64 (a, n);
#else
    return simd128i_set_lohi (simd64i_shl64 (simd128i_lo (a), n),
			      simd64i_shl64 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shr8 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_and_si128 (_mm_srli_epi32 (a, n), _mm_set1_epi8 (0xff >> n));
#else
    return simd128i_set_lohi (simd64i_shr8 (simd128i_lo (a), n),
			      simd64i_shr8 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shr16 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_srli_epi16 (a, n);
#else
    return simd128i_set_lohi (simd64i_shr16 (simd128i_lo (a), n),
			      simd64i_shr16 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shr32 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_srli_epi32 (a, n);
#else
    return simd128i_set_lohi (simd64i_shr32 (simd128i_lo (a), n),
			      simd64i_shr32 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_sra32 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_srai_epi32 (a, n);
#else
    return simd128i_set_lohi (simd64i_sra32 (simd128i_lo (a), n),
			      simd64i_sra32 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_shr64 (simd128i a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_srli_epi64 (a, n);
#else
    return simd128i_set_lohi (simd64i_shr64 (simd128i_lo (a), n),
			      simd64i_shr64 (simd128i_hi (a), n));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_min8u (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_min_epu8 (a, b);
#else
    return simd128i_set_lohi (simd64i_min8u (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_min8u (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_min16u (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_min_epu16 (a, b);
#else
    return simd128i_set_lohi (simd64i_min16u (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_min16u (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_min32u (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_min_epu32 (a, b);
#else
    return simd128i_set_lohi (simd64i_min32u (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_min32u (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_min32s (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_min_epi32 (a, b);
#else
    return simd128i_set_lohi (simd64i_min32s (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_min32s (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_max8u (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_max_epu8 (a, b);
#else
    return simd128i_set_lohi (simd64i_max8u (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_max8u (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_max16u (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_max_epu16 (a, b);
#else
    return simd128i_set_lohi (simd64i_max16u (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_max16u (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_max32u (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_max_epu32 (a, b);
#else
    return simd128i_set_lohi (simd64i_max32u (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_max32u (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_max32s (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_max_epi32 (a, b);
#else
    return simd128i_set_lohi (simd64i_max32s (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_max32s (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_cmpeq8 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_cmpeq_epi8 (a, b);
#else
    return simd128i_set_lohi (simd64i_cmpeq8 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_cmpeq8 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_cmpeq16 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_cmpeq_epi16 (a, b);
#else
    return simd128i_set_lohi (simd64i_cmpeq16 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_cmpeq16 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_cmpeq32 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_cmpeq_epi32 (a, b);
#else
    return simd128i_set_lohi (simd64i_cmpeq32 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_cmpeq32 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128i_cmplt32 (simd128i a, simd128i b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_cmplt_epi32 (a, b);
#else
    return simd128i_set_lohi (simd64i_cmplt32 (simd128i_lo (a), simd128i_lo (b)),
			      simd64i_cmplt32 (simd128i_hi (a), simd128i_hi (b)));
#endif
}

__SIMD_CONST_FUNC simd128i simd128i_gather8x16 (const void *base,
					       simd128i a0,
					       simd128i a1,
					       simd128i a2,
					       simd128i a3)
{
#if __SIMD_NEON
#error
#else
    return simd128i_set_lohi (simd64i_gather8x8 (base,
						 simd128i_lo (a0),
						 simd128i_hi (a0),
						 simd128i_lo (a1),
						 simd128i_hi (a1)),
			      simd64i_gather8x8 (base,
						 simd128i_lo (a2),
						 simd128i_hi (a2),
						 simd128i_lo (a3),
						 simd128i_hi (a3)));
#endif
}

__SIMD_CONST_FUNC simd128i simd128i_gather8a32x4 (const void *base,
						  simd128i a)
{
#if __SIMD_NEON
#error
#else
    return simd128i_set_lohi (simd64i_gather8a32x2 (base, simd128i_lo (a)),
			      simd64i_gather8a32x2 (base, simd128i_hi (a)));
#endif
}

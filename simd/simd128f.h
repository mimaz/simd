#pragma once

__SIMD_PURE_FUNC simd64f simd128f_lo (simd128f a)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    union {
	int64_t i;
	simd64f f;
    } u = { _mm_extract_epi64 (_mm_castps_si128 (a), 0) };

    return u.f;
#else
    return a.l;
#endif
}

__SIMD_PURE_FUNC simd64f simd128f_hi (simd128f a)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    union {
	int64_t i;
	simd64f f;
    } u = { _mm_extract_epi64 (_mm_castps_si128 (a), 1) };

    return u.f;
#else
    return a.h;
#endif
}

__SIMD_PURE_FUNC float simd128f_get (simd128f a, unsigned n)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_extract_ps (a, n);
#else
    if (n < 2) {
	return simd64f_get (simd128f_lo (a), n);
    }

    return simd64f_get (simd128f_hi (a), n - 2);
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_set_lohi (simd64f l, simd64f h)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set_ps (simd64f_hi (h), simd64f_lo (h),
		       simd64f_hi (l), simd64f_lo (l));
#else
    return (simd128f) { l, h };
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_set (float v0, float v1, float v2, float v3)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set_ps (v3, v2, v1, v0);
#else
    return simd128f_set_lohi (simd64f_set (v0, v1), simd64f_set (v2, v3));
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_dup (float v)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_set1_ps (v);
#else
    return simd128f_set (v, v, v, v);
#endif
}

__SIMD_CONST_FUNC simd128f simd128f_load (const void *p)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_loadu_ps (p);
#else
    return *(const simd128f *) p;
#endif
}

__SIMD_FUNC void simd128f_store (simd128f a, void *p)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    _mm_storeu_ps (p, a);
#else
    *(simd128f *) p = a;
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_add (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_add_ps (a, b);
#else
    return simd128f_set_lohi (simd64f_add (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_add (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_sub (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_sub_ps (a, b);
#else
    return simd128f_set_lohi (simd64f_sub (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_sub (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_mul (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_mul_ps (a, b);
#else
    return simd128f_set_lohi (simd64f_mul (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_mul (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_div (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_div_ps (a, b);
#else
    return simd128f_set_lohi (simd64f_div (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_div (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_scale (simd128f a, float s)
{
    return simd128f_mul (a, simd128f_dup (s));
}

__SIMD_PURE_FUNC simd128f simd128f_min (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_min_ps (a, b);
#else
    return simd128f_set_lohi (simd64f_min (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_min (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128f simd128f_max (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_max_ps (a, b);
#else
    return simd128f_set_lohi (simd64f_max (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_max (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128f_cmpeq (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_castps_si128 (_mm_cmpeq_ps (a, b));
#else
    return simd128i_set_lohi (simd64f_cmpeq (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_cmpeq (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128f_cmplt (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_castps_si128 (_mm_cmplt_ps (a, b));
#else
    return simd128i_set_lohi (simd64f_cmplt (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_cmplt (simd128f_hi (a), simd128f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd128i simd128f_cmple (simd128f a, simd128f b)
{
#if __SIMD_NEON
#error
#elif __SIMD_SSE42
    return _mm_castps_si128 (_mm_cmple_ps (a, b));
#else
    return simd128i_set_lohi (simd64f_cmple (simd128f_lo (a), simd128f_lo (b)),
			      simd64f_cmple (simd128f_hi (a), simd128f_hi (b)));
#endif
}

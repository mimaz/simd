#pragma once

__SIMD_PURE_FUNC simd128i simd256i_lo (simd256i a)
{
    return a.l;
}

__SIMD_PURE_FUNC simd128i simd256i_hi (simd256i a)
{
    return a.h;
}

__SIMD_PURE_FUNC uint8_t simd256i_get8 (simd256i a, unsigned n)
{
    if (n < 16) {
	return simd128i_get8 (simd256i_lo (a), n);
    }

    return simd128i_get8 (simd256i_hi (a), n - 16);
}

__SIMD_PURE_FUNC uint32_t simd256i_get32 (simd256i a, unsigned n)
{
    if (n < 4) {
	return simd128i_get32 (simd256i_lo (a), n);
    }

    return simd128i_get32 (simd256i_hi (a), n - 4);
}

__SIMD_PURE_FUNC uint64_t simd256i_get64 (simd256i a, unsigned n)
{
    if (n < 2) {
	return simd128i_get64 (simd256i_lo (a), n);
    }

    return simd128i_get64 (simd256i_hi (a), n - 2);
}

__SIMD_PURE_FUNC simd256i simd256i_set_lohi (simd128i l, simd128i h)
{
    return (simd256i) { l, h };
}

__SIMD_PURE_FUNC simd256i simd256i_set32 (uint32_t v0, uint32_t v1,
					  uint32_t v2, uint32_t v3,
					  uint32_t v4, uint32_t v5,
					  uint32_t v6, uint32_t v7)
{
    return simd256i_set_lohi (simd128i_set32 (v0, v1, v2, v3),
			      simd128i_set32 (v4, v5, v6, v7));
}

__SIMD_PURE_FUNC simd256i simd256i_dup8 (uint8_t v)
{
    return simd256i_set_lohi (simd128i_dup8 (v), simd128i_dup8 (v));
}

__SIMD_PURE_FUNC simd256i simd256i_dup32 (uint32_t v)
{
    return simd256i_set_lohi (simd128i_dup32 (v), simd128i_dup32 (v));
}

__SIMD_CONST_FUNC simd256i simd256i_load (const void *p)
{
    const simd128i *h = p;

    return simd256i_set_lohi (simd128i_load (h + 0),
			      simd128i_load (h + 1));
}

__SIMD_FUNC void simd256i_store (simd256i a, void *p)
{
    simd128i_store (simd256i_lo (a), (simd128i *) p + 0);
    simd128i_store (simd256i_hi (a), (simd128i *) p + 1);
}

__SIMD_PURE_FUNC simd256i simd256i_add8 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_add8 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_add8 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_add32 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_add32 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_add32 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_sub8 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_sub8 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_sub8 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_sub32 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_sub32 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_sub32 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_mul32 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_mul32 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_mul32 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_div32 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_div32 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_div32 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_or (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_or (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_or (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_and (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_and (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_and (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_andnot (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_andnot (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_andnot (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_xor (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_xor (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_xor (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_neg8 (simd256i a)
{
    return simd256i_set_lohi (simd128i_neg8 (simd256i_lo (a)),
			      simd128i_neg8 (simd256i_hi (a)));
}

__SIMD_PURE_FUNC simd256i simd256i_sign8 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_sign8 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_sign8 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_blend8 (simd256i a, simd256i b, simd256i m)
{
    return simd256i_set_lohi (simd128i_blend8 (simd256i_lo (a), simd256i_lo (b),
					       simd256i_lo (m)),
			      simd128i_blend8 (simd256i_hi (a), simd256i_hi (b),
					       simd256i_hi (m)));
}

__SIMD_PURE_FUNC simd256i simd256i_shl8 (simd256i a, unsigned n)
{
    return simd256i_set_lohi (simd128i_shl8 (simd256i_lo (a), n),
			      simd128i_shl8 (simd256i_hi (a), n));
}

__SIMD_PURE_FUNC simd256i simd256i_shl32 (simd256i a, unsigned n)
{
    return simd256i_set_lohi (simd128i_shl32 (simd256i_lo (a), n),
			      simd128i_shl32 (simd256i_hi (a), n));
}

__SIMD_PURE_FUNC simd256i simd256i_shr8 (simd256i a, unsigned n)
{
    return simd256i_set_lohi (simd128i_shr8 (simd256i_lo (a), n),
			      simd128i_shr8 (simd256i_hi (a), n));
}

__SIMD_PURE_FUNC simd256i simd256i_shr32 (simd256i a, unsigned n)
{
    return simd256i_set_lohi (simd128i_shr32 (simd256i_lo (a), n),
			      simd128i_shr32 (simd256i_hi (a), n));
}

__SIMD_PURE_FUNC simd256i simd256i_sra32 (simd256i a, unsigned n)
{
    return simd256i_set_lohi (simd128i_sra32 (simd256i_lo (a), n),
			      simd128i_sra32 (simd256i_hi (a), n));
}

__SIMD_PURE_FUNC simd256i simd256i_min32u (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_min32u (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_min32u (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_max8u (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_max8u (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_max8u (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_PURE_FUNC simd256i simd256i_cmplt32 (simd256i a, simd256i b)
{
    return simd256i_set_lohi (simd128i_cmplt32 (simd256i_lo (a), simd256i_lo (b)),
			      simd128i_cmplt32 (simd256i_hi (a), simd256i_hi (b)));
}

__SIMD_CONST_FUNC simd256i simd256i_gather8x32 (const void *base,
					       simd256i a0,
					       simd256i a1,
					       simd256i a2,
					       simd256i a3)
{
    return simd256i_set_lohi (simd128i_gather8x16 (base,
						   simd256i_lo (a0),
						   simd256i_hi (a0),
						   simd256i_lo (a1),
						   simd256i_hi (a1)),
			      simd128i_gather8x16 (base,
						   simd256i_lo (a2),
						   simd256i_hi (a2),
						   simd256i_lo (a3),
						   simd256i_hi (a3)));
}

__SIMD_CONST_FUNC simd256i simd256i_gather8a32x8 (const void *base,
						  simd256i a)
{
    return simd256i_set_lohi (simd128i_gather8a32x4 (base, simd256i_lo (a)),
			      simd128i_gather8a32x4 (base, simd256i_hi (a)));
}

#pragma once

__SIMD_PURE_FUNC float simd64f_lo (simd64f a)
{
#if __SIMD_NEON
#error
#else
    return a.l;
#endif
}

__SIMD_PURE_FUNC float simd64f_hi (simd64f a)
{
#if __SIMD_NEON
#error
#else
    return a.h;
#endif
}

__SIMD_PURE_FUNC float simd64f_get (simd64f a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    switch (n) {
    case 0:
	return simd64f_lo (a);

    case 1:
	return simd64f_hi (a);

    default:
	__builtin_unreachable ();
    }
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_set (float v0, float v1)
{
#if __SIMD_NEON
#error
#else
    return (simd64f) { v0, v1, };
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_dup (float v)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (v, v);
#endif
}

__SIMD_CONST_FUNC simd64f simd64f_load (const void *p)
{
#if __SIMD_NEON
#error
#else
    return *(const simd64f *) p;
#endif
}

__SIMD_FUNC void simd64f_store (simd64f a, void *p)
{
#if __SIMD_NEON
#error
#else
    *(simd64f *) p = a;
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_add (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (simd64f_lo (a) + simd64f_lo (b),
			simd64f_hi (a) + simd64f_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_sub (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (simd64f_lo (a) - simd64f_lo (b),
			simd64f_hi (a) - simd64f_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_mul (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (simd64f_lo (a) * simd64f_lo (b),
			simd64f_hi (a) * simd64f_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_div (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (simd64f_lo (a) / simd64f_lo (b),
			simd64f_hi (a) / simd64f_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_scale (simd64f a, float s)
{
    return simd64f_mul (a, simd64f_dup (s));
}

__SIMD_PURE_FUNC simd64f simd64f_min (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (__SIMD_MIN_S (simd64f_lo (a), simd64f_lo (b)),
			__SIMD_MIN_S (simd64f_hi (a), simd64f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd64f simd64f_max (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (__SIMD_MAX_S (simd64f_lo (a), simd64f_lo (b)),
			__SIMD_MAX_S (simd64f_hi (a), simd64f_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd64i simd64f_cmpeq (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64f_lo (a) == simd64f_lo (b) ? 0xffffffff : 0,
			  simd64f_hi (a) == simd64f_hi (b) ? 0xffffffff : 0);
#endif
}

__SIMD_PURE_FUNC simd64i simd64f_cmplt (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64f_lo (a) < simd64f_lo (b) ? 0xffffffff : 0,
			  simd64f_hi (a) < simd64f_hi (b) ? 0xffffffff : 0);
#endif
}

__SIMD_PURE_FUNC simd64i simd64f_cmple (simd64f a, simd64f b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64f_lo (a) <= simd64f_lo (b) ? 0xffffffff : 0,
			  simd64f_hi (a) <= simd64f_hi (b) ? 0xffffffff : 0);
#endif
}

__SIMD_PURE_FUNC simd64i simd64f_cvti32 (simd64f a)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64f_lo (a), simd64f_hi (a));
#endif
}

__SIMD_PURE_FUNC float simd64f_dot (simd64f a, simd64f b)
{
    return simd64f_lo (a) * simd64f_lo (b) + simd64f_hi (a) * simd64f_hi (b);
}

__SIMD_PURE_FUNC float simd64f_len2 (simd64f a)
{
    return simd64f_dot (a, a);
}

__SIMD_PURE_FUNC float simd64f_len (simd64f a)
{
    return sqrtf (simd64f_len2 (a));
}

__SIMD_PURE_FUNC simd64f simd64f_norm (simd64f a)
{
    return simd64f_mul (a, simd64f_dup (1.0f / simd64f_len (a)));
}

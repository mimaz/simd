#pragma once

#include <nmmintrin.h>
#include <stdint.h>
#include <math.h>

#ifdef __SSE4_2__
#define __SIMD_SSE42 1
#endif

#if __SIMD_ARM64NEON
#error
#elif __SIMD_SSE42
#define __SIMD_HAS_SIMD128F 1
#define __SIMD_HAS_SIMD128I 1
typedef __m128 simd128f;
typedef __m128i simd128i;
#else
#endif

#if !__SIMD_HAS_SIMD64F
typedef struct {
    float l, h;
} __attribute__((aligned (8))) simd64f;
#endif

#if !__SIMD_HAS_SIMD64I
typedef uint64_t simd64i;
#endif

#if !__SIMD_HAS_SIMD128F
typedef struct {
    simd64f l, h;
} __attribute__((aligned (16))) simd128f;
#endif

#if !__SIMD_HAS_SIMD128I
typedef struct {
    simd64i l, h;
} __attribute__((aligned (16))) simd128i;
#endif

#if !__SIMD_HAS_SIMD256I
typedef struct {
    simd128i l, h;
} __attribute__((aligned (16))) simd256i;
#endif

#if !__SIMD_HAS_SIMD512I
typedef struct {
    simd256i l, h;
} __attribute__((aligned (16))) simd512i;
#endif

#define __SIMD_MIN_S(a, b)  ((a) < (b) ? (a) : (b))
#define __SIMD_MAX_S(a, b)  ((a) > (b) ? (a) : (b))
#define __SIMD_CONST_FUNC   __attribute__((always_inline, const)) static inline
#define __SIMD_PURE_FUNC    __attribute__((always_inline, pure)) static inline
#define __SIMD_FUNC	    __attribute__((always_inline)) static inline
#define __SIMD_FORWARD_FUNC static

_Static_assert (sizeof (simd64f) == sizeof (simd64i), "sizeof");

__SIMD_PURE_FUNC simd64i simd64f_as64i (simd64f a)
{
#if __SIMD_NEON
#error
#else
    return ((union { simd64f f; simd64i i; }) { a }).i;
#endif
}

__SIMD_PURE_FUNC simd64f simd64i_as64f (simd64i a)
{
#if __SIMD_NEON
#error
#else
    return ((union { simd64i i; simd64f f; }) { a }).f;
#endif
}

__SIMD_PURE_FUNC simd128i simd128f_as128i (simd128f a)
{
#if __SIMD_NEON
#error
#else
    return ((union { simd128f f; simd128i i; }) { a }).i;
#endif
}

__SIMD_PURE_FUNC simd128f simd128i_as128f (simd128i a)
{
#if __SIMD_NEON
#error
#else
    return ((union { simd128i i; simd128f f; }) { a }).f;
#endif
}

#include "simd64i.h"
#include "simd64f.h"
#include "simd128i.h"
#include "simd128f.h"
#include "simd256i.h"
#include "simd512i.h"

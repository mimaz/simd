#pragma once

__SIMD_PURE_FUNC simd256i simd512i_lo (simd512i a)
{
    return a.l;
}

__SIMD_PURE_FUNC simd256i simd512i_hi (simd512i a)
{
    return a.h;
}

__SIMD_PURE_FUNC uint8_t simd512i_get8 (simd512i a, unsigned n)
{
    if (n < 32) {
	return simd256i_get8 (simd512i_lo (a), n);
    }

    return simd256i_get8 (simd512i_hi (a), n - 32);
}

__SIMD_PURE_FUNC uint32_t simd512i_get32 (simd512i a, unsigned n)
{
    if (n < 8) {
	return simd256i_get32 (simd512i_lo (a), n);
    }

    return simd256i_get32 (simd512i_hi (a), n - 8);
}

__SIMD_PURE_FUNC uint64_t simd512i_get64 (simd512i a, unsigned n)
{
    if (n < 4) {
	return simd256i_get64 (simd512i_lo (a), n);
    }

    return simd256i_get64 (simd512i_hi (a), n - 4);
}

__SIMD_PURE_FUNC simd512i simd512i_set_lohi (simd256i l, simd256i h)
{
    return (simd512i) { l, h };
}

__SIMD_PURE_FUNC simd512i simd512i_set32 (uint32_t v0, uint32_t v1,
					  uint32_t v2, uint32_t v3,
					  uint32_t v4, uint32_t v5,
					  uint32_t v6, uint32_t v7,
					  uint32_t v8, uint32_t v9,
					  uint32_t v10, uint32_t v11,
					  uint32_t v12, uint32_t v13,
					  uint32_t v14, uint32_t v15)
{
    return simd512i_set_lohi (simd256i_set32 (v0, v1, v2, v3,
					      v4, v5, v6, v7),
			      simd256i_set32 (v8, v9, v10, v11,
					      v12, v13, v14, v15));
}

__SIMD_PURE_FUNC simd512i simd512i_dup8 (uint8_t v)
{
    return simd512i_set_lohi (simd256i_dup8 (v), simd256i_dup8 (v));
}

__SIMD_PURE_FUNC simd512i simd512i_dup32 (uint32_t v)
{
    return simd512i_set_lohi (simd256i_dup32 (v), simd256i_dup32 (v));
}

__SIMD_CONST_FUNC simd512i simd512i_load (const void *p)
{
    const simd256i *h = p;

    return simd512i_set_lohi (simd256i_load (h + 0),
			      simd256i_load (h + 1));
}

__SIMD_FUNC void simd512i_store (simd512i a, void *p)
{
    simd256i_store (simd512i_lo (a), (simd256i *) p + 0);
    simd256i_store (simd512i_hi (a), (simd256i *) p + 1);
}

__SIMD_PURE_FUNC simd512i simd512i_add8 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_add8 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_add8 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_add32 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_add32 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_add32 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_sub8 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_sub8 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_sub8 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_sub32 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_sub32 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_sub32 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_mul32 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_mul32 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_mul32 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_div32 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_div32 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_div32 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_or (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_or (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_or (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_and (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_and (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_and (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_andnot (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_andnot (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_andnot (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_xor (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_xor (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_xor (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_neg8 (simd512i a)
{
    return simd512i_set_lohi (simd256i_neg8 (simd512i_lo (a)),
			      simd256i_neg8 (simd512i_hi (a)));
}

__SIMD_PURE_FUNC simd512i simd512i_sign8 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_sign8 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_sign8 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_blend8 (simd512i a, simd512i b, simd512i m)
{
    return simd512i_set_lohi (simd256i_blend8 (simd512i_lo (a), simd512i_lo (b),
					       simd512i_lo (m)),
			      simd256i_blend8 (simd512i_hi (a), simd512i_hi (b),
					       simd512i_hi (m)));
}

__SIMD_PURE_FUNC simd512i simd512i_shl8 (simd512i a, unsigned n)
{
    return simd512i_set_lohi (simd256i_shl8 (simd512i_lo (a), n),
			      simd256i_shl8 (simd512i_hi (a), n));
}

__SIMD_PURE_FUNC simd512i simd512i_shl32 (simd512i a, unsigned n)
{
    return simd512i_set_lohi (simd256i_shl32 (simd512i_lo (a), n),
			      simd256i_shl32 (simd512i_hi (a), n));
}

__SIMD_PURE_FUNC simd512i simd512i_shr8 (simd512i a, unsigned n)
{
    return simd512i_set_lohi (simd256i_shr8 (simd512i_lo (a), n),
			      simd256i_shr8 (simd512i_hi (a), n));
}

__SIMD_PURE_FUNC simd512i simd512i_shr32 (simd512i a, unsigned n)
{
    return simd512i_set_lohi (simd256i_shr32 (simd512i_lo (a), n),
			      simd256i_shr32 (simd512i_hi (a), n));
}

__SIMD_PURE_FUNC simd512i simd512i_sra32 (simd512i a, unsigned n)
{
    return simd512i_set_lohi (simd256i_sra32 (simd512i_lo (a), n),
			      simd256i_sra32 (simd512i_hi (a), n));
}

__SIMD_PURE_FUNC simd512i simd512i_min32u (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_min32u (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_min32u (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_max8u (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_max8u (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_max8u (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_PURE_FUNC simd512i simd512i_cmplt32 (simd512i a, simd512i b)
{
    return simd512i_set_lohi (simd256i_cmplt32 (simd512i_lo (a), simd512i_lo (b)),
			      simd256i_cmplt32 (simd512i_hi (a), simd512i_hi (b)));
}

__SIMD_CONST_FUNC simd512i simd512i_gather8x64 (const void *base,
						simd512i a0,
						simd512i a1,
						simd512i a2,
						simd512i a3)
{
    return simd512i_set_lohi (simd256i_gather8x32 (base,
						   simd512i_lo (a0),
						   simd512i_hi (a0),
						   simd512i_lo (a1),
						   simd512i_hi (a1)),
			      simd256i_gather8x32 (base,
						   simd512i_lo (a2),
						   simd512i_hi (a2),
						   simd512i_lo (a3),
						   simd512i_hi (a3)));
}


__SIMD_CONST_FUNC simd512i simd512i_gather8a32x16 (const void *base,
						   simd512i a)
{
    return simd512i_set_lohi (simd256i_gather8a32x8 (base, simd512i_lo (a)),
			      simd256i_gather8a32x8 (base, simd512i_hi (a)));
}

__SIMD_PURE_FUNC simd512i simd512i_expandu_8to32 (simd128i a)
{
    union {
	simd128i a;
	uint8_t v[16];
    } _ = { a };

    return simd512i_set32 (_.v[0], _.v[1], _.v[2], _.v[3],
			   _.v[4], _.v[5], _.v[6], _.v[7],
			   _.v[8], _.v[9], _.v[10], _.v[11],
			   _.v[12], _.v[13], _.v[14], _.v[15]);
}

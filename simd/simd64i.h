#pragma once

__SIMD_PURE_FUNC uint32_t simd64i_lo (simd64i a)
{
#if __SIMD_NEON
#error
#else
    return a >> 0;
#endif
}

__SIMD_PURE_FUNC uint32_t simd64i_hi (simd64i a)
{
#if __SIMD_NEON
#error
#else
    return a >> 32;
#endif
}

__SIMD_PURE_FUNC uint8_t simd64i_get8 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    return a >> (n * 8);
#endif
}

__SIMD_PURE_FUNC uint16_t simd64i_get16 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    return a >> (n * 16);
#endif
}

__SIMD_PURE_FUNC uint32_t simd64i_get32 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    return a >> (n * 32);
#endif
}

__SIMD_PURE_FUNC uint64_t simd64i_get64 (simd64i a)
{
#if __SIMD_NEON
#error
#else
    return a;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_set8 (uint8_t v0, uint8_t v1, uint8_t v2, uint8_t v3,
				       uint8_t v4, uint8_t v5, uint8_t v6, uint8_t v7)
{
#if __SIMD_NEON
#error
#else
    union {
	uint8_t a[8];
	uint64_t v;
    } u = { .a = { v0, v1, v2, v3, v4, v5, v6, v7 } };

    return u.v;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_set16 (uint16_t v0, uint16_t v1, uint16_t v2, uint16_t v3)
{
#if __SIMD_NEON
#error
#else
    union {
	uint16_t a[4];
	uint64_t v;
    } u = { .a = { v0, v1, v2, v3 } };

    return u.v;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_set32 (uint32_t v0, uint32_t v1)
{
#if __SIMD_NEON
#error
#else
    union {
	uint32_t a[2];
	uint64_t v;
    } u = { .a = { v0, v1 } };

    return u.v;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_set64 (uint64_t v)
{
#if __SIMD_NEON
#error
#else
    return v;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_dup8 (uint8_t v)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set8 (v, v, v, v, v, v, v, v);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_dup16 (uint16_t v)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set16 (v, v, v, v);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_dup32 (uint32_t v)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (v, v);
#endif
}

__SIMD_CONST_FUNC simd64i simd64i_load (const void *p)
{
#if __SIMD_NEON
#error
#else
    return *(const simd64i *) p;
#endif
}

__SIMD_FUNC void simd64i_store (simd64i a, void *p)
{
#if __SIMD_NEON
#error
#else
    *(simd64i *) p = a;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_add8 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x00ff00ff00ff00ff;
    const uint64_t odmask = 0xff00ff00ff00ff00;

    const uint64_t ev = ((a & evmask) + (b & evmask)) & evmask;
    const uint64_t od = ((a & odmask) + (b & odmask)) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_add16 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x0000ffff0000ffff;
    const uint64_t odmask = 0xffff0000ffff0000;;

    const uint64_t ev = ((a & evmask) + (b & evmask)) & evmask;
    const uint64_t od = ((a & odmask) + (b & odmask)) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_add32 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64i_lo (a) + simd64i_lo (b),
			  simd64i_hi (a) + simd64i_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_sub8 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x00ff00ff00ff00ff;
    const uint64_t odmask = 0xff00ff00ff00ff00;

    const uint64_t ev = (((a & evmask) | odmask) - (b & evmask)) & evmask;
    const uint64_t od = (((a & odmask) | evmask) - (b & odmask)) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_sub16 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x0000ffff0000ffff;
    const uint64_t odmask = 0xffff0000ffff0000;

    const uint64_t ev = (((a & evmask) | odmask) - (b & evmask)) & evmask;
    const uint64_t od = (((a & odmask) | evmask) - (b & odmask)) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_sub32 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64i_lo (a) - simd64i_lo (b),
			  simd64i_hi (a) - simd64i_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_mul32 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 ((int32_t) simd64i_lo (a) * (int32_t) simd64i_lo (b),
			  (int32_t) simd64i_hi (a) * (int32_t) simd64i_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_div32 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 ((int32_t) simd64i_lo (a) / (int32_t) simd64i_lo (b),
			  (int32_t) simd64i_hi (a) / (int32_t) simd64i_hi (b));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_or (simd64i a, simd64i b)
{
    return simd64i_set64 (simd64i_get64 (a) | simd64i_get64 (b));
}

__SIMD_PURE_FUNC simd64i simd64i_and (simd64i a, simd64i b)
{
    return simd64i_set64 (simd64i_get64 (a) & simd64i_get64 (b));
}

__SIMD_PURE_FUNC simd64i simd64i_andnot (simd64i a, simd64i b)
{
    return ~a & b;
}

__SIMD_PURE_FUNC simd64i simd64i_xor (simd64i a, simd64i b)
{
    return simd64i_set64 (simd64i_get64 (a) ^ simd64i_get64 (b));
}

__SIMD_PURE_FUNC simd64i simd64i_neg8 (simd64i a)
{
    /* TODO */
    return a;
}

__SIMD_PURE_FUNC simd64i simd64i_sign8 (simd64i a, simd64i b)
{
    /* TODO no zero case */
    uint64_t nmask = a & 0x8080808080808080;

    nmask |= nmask >> 1;
    nmask |= nmask >> 2;
    nmask |= nmask >> 4;

    return simd64i_add8 (a ^ nmask, 0x0101010101010101 & nmask);
}

__SIMD_PURE_FUNC simd64i simd64i_blend8 (simd64i a, simd64i b, simd64i m)
{
    return simd64i_or (simd64i_andnot (a, m), simd64i_and (b, m));
}

__SIMD_PURE_FUNC simd64i simd64i_shl8 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x00ff00ff00ff00ff;
    const uint64_t odmask = 0xff00ff00ff00ff00;

    n = __SIMD_MAX_S (n, 8);

    const uint64_t ev = (a << n) & evmask;
    const uint64_t od = (a << n) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_shl16 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x0000ffff0000ffff;
    const uint64_t odmask = 0xffff0000ffff0000;

    n = __SIMD_MAX_S (n, 16);

    const uint64_t ev = (a << n) & evmask;
    const uint64_t od = (a << n) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_shl32 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    uint64_t m32 = (uint32_t) -1 << n;

    return (a << n) & (m32 | (m32 << 32));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_shl64 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    return a << n;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_shr8 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x00ff00ff00ff00ff;
    const uint64_t odmask = 0xff00ff00ff00ff00;

    n = __SIMD_MIN_S (n, 8);

    const uint64_t ev = ((a & evmask) >> n) & evmask;
    const uint64_t od = ((a & odmask) >> n) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_shr16 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x0000ffff0000ffff;
    const uint64_t odmask = 0xffff0000ffff0000;

    n = __SIMD_MIN_S (n, 16);

    const uint64_t ev = ((a & evmask) >> n) & evmask;
    const uint64_t od = ((a & odmask) >> n) & odmask;

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_shr32 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    uint64_t m32 = (uint32_t) -1 >> n;

    return (a >> n) & (m32 | (m32 << 32));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_sra32 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (((union { int64_t _; int32_t a[2]; }) { a }).a[0] >> n,
			  ((union { int64_t _; int32_t a[2]; }) { a }).a[1] >> n);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_shr64 (simd64i a, unsigned n)
{
#if __SIMD_NEON
#error
#else
    return a >> n;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_min8u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x00ff00ff00ff00ff;
    const uint64_t odmask = 0xff00ff00ff00ff00;

    const uint64_t evbit = (((((a & evmask) | odmask) - (b & evmask)) & odmask) >> 1) & evmask;
    const uint64_t odbit = (((((a & odmask) | evmask) - (b & odmask)) & evmask) >> 1) & odmask;


    const uint64_t evbit2 = evbit | (evbit >> 1);
    const uint64_t odbit2 = odbit | (odbit >> 1);

    const uint64_t evbit4 = evbit2 | (evbit2 >> 2);
    const uint64_t odbit4 = odbit2 | (odbit2 >> 2);

    const uint64_t evbit8 = evbit4 | (evbit4 >> 4);
    const uint64_t odbit8 = odbit4 | (odbit4 >> 4);

    const uint64_t ev = (a & ~evbit8 & evmask) | (b & evbit8);
    const uint64_t od = (a & ~odbit8 & odmask) | (b & odbit8);

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_min16u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x0000ffff0000ffff;
    const uint64_t odmask = 0xffff0000ffff0000;

    const uint64_t evbit = (((((a & evmask) | odmask) - (b & evmask)) & odmask) >> 1) & evmask;
    const uint64_t odbit = (((((a & odmask) | evmask) - (b & odmask)) & evmask) >> 1) & odmask;

    const uint64_t evbit2 = evbit | (evbit >> 1);
    const uint64_t odbit2 = odbit | (odbit >> 1);

    const uint64_t evbit4 = evbit2 | (evbit2 >> 2);
    const uint64_t odbit4 = odbit2 | (odbit2 >> 2);

    const uint64_t evbit8 = evbit4 | (evbit4 >> 4);
    const uint64_t odbit8 = odbit4 | (odbit4 >> 4);

    const uint64_t evbit16 = evbit8 | (evbit4 >> 8);
    const uint64_t odbit16 = odbit8 | (odbit4 >> 8);

    const uint64_t ev = (a & ~evbit16 & evmask) | (b & evbit8);
    const uint64_t od = (a & ~odbit16 & odmask) | (b & odbit8);

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_min32u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (__SIMD_MIN_S (simd64i_lo (a), simd64i_lo (b)),
			  __SIMD_MIN_S (simd64i_hi (a), simd64i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_min32s (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (__SIMD_MIN_S ((int32_t) simd64i_lo (a), (int32_t) simd64i_lo (b)),
			  __SIMD_MIN_S ((int32_t) simd64i_hi (a), (int32_t) simd64i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_min64u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return __SIMD_MIN_S (a, b);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_min64s (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return __SIMD_MIN_S ((int64_t) a, (int64_t) b);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_max8u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x00ff00ff00ff00ff;
    const uint64_t odmask = 0xff00ff00ff00ff00;

    const uint64_t evbit = (((((a & evmask) | odmask) - (b & evmask)) & odmask) >> 1) & evmask;
    const uint64_t odbit = (((((a & odmask) | evmask) - (b & odmask)) & evmask) >> 1) & odmask;


    const uint64_t evbit2 = evbit | (evbit >> 1);
    const uint64_t odbit2 = odbit | (odbit >> 1);

    const uint64_t evbit4 = evbit2 | (evbit2 >> 2);
    const uint64_t odbit4 = odbit2 | (odbit2 >> 2);

    const uint64_t evbit8 = evbit4 | (evbit4 >> 4);
    const uint64_t odbit8 = odbit4 | (odbit4 >> 4);

    const uint64_t ev = (a & evbit8) | (b & ~evbit8 & evmask);
    const uint64_t od = (a & odbit8) | (b & ~odbit8 & odmask);

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_max16u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x0000ffff0000ffff;
    const uint64_t odmask = 0xffff0000ffff0000;

    const uint64_t evbit = (((((a & evmask) | odmask) - (b & evmask)) & odmask) >> 1) & evmask;
    const uint64_t odbit = (((((a & odmask) | evmask) - (b & odmask)) & evmask) >> 1) & odmask;

    const uint64_t evbit2 = evbit | (evbit >> 1);
    const uint64_t odbit2 = odbit | (odbit >> 1);

    const uint64_t evbit4 = evbit2 | (evbit2 >> 2);
    const uint64_t odbit4 = odbit2 | (odbit2 >> 2);

    const uint64_t evbit8 = evbit4 | (evbit4 >> 4);
    const uint64_t odbit8 = odbit4 | (odbit4 >> 4);

    const uint64_t evbit16 = evbit8 | (evbit4 >> 8);
    const uint64_t odbit16 = odbit8 | (odbit4 >> 8);

    const uint64_t ev = (a & evbit16) | (b & ~evbit8 & evmask);
    const uint64_t od = (a & odbit16) | (b & ~odbit8 & odmask);

    return ev | od;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_max32u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (__SIMD_MAX_S (simd64i_lo (a), simd64i_lo (b)),
			  __SIMD_MAX_S (simd64i_hi (a), simd64i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_max32s (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (__SIMD_MAX_S ((int32_t) simd64i_lo (a),
					(int32_t) simd64i_lo (b)),
			  __SIMD_MAX_S ((int32_t) simd64i_hi (a),
					(int32_t) simd64i_hi (b)));
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_max64u (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return __SIMD_MAX_S (a, b);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_max64s (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return __SIMD_MAX_S ((int64_t) a, (int64_t) b);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_cmpeq8 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x00ff00ff00ff00ff;
    const uint64_t odmask = 0xff00ff00ff00ff00;

    const uint64_t evbit1 = (((odmask - ((a ^ b) & evmask)) & odmask) >> 1) & evmask;
    const uint64_t odbit1 = (((evmask - ((a ^ b) & odmask)) & evmask) >> 1) & odmask;

    const uint64_t evbit2 = evbit1 | (evbit1 >> 1);
    const uint64_t odbit2 = odbit1 | (odbit1 >> 1);

    const uint64_t evbit4 = evbit2 | (evbit2 >> 2);
    const uint64_t odbit4 = odbit2 | (odbit2 >> 2);

    const uint64_t evbit8 = evbit4 | (evbit4 >> 4);
    const uint64_t odbit8 = odbit4 | (odbit4 >> 4);

    return evbit8 | odbit8;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_cmpeq16 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    const uint64_t evmask = 0x0000ffff0000ffff;
    const uint64_t odmask = 0xffff0000ffff0000;

    const uint64_t evbit1 = (((odmask - ((a ^ b) & evmask)) & odmask) >> 1) & evmask;
    const uint64_t odbit1 = (((evmask - ((a ^ b) & odmask)) & evmask) >> 1) & odmask;

    const uint64_t evbit2 = evbit1 | (evbit1 >> 1);
    const uint64_t odbit2 = odbit1 | (odbit1 >> 1);

    const uint64_t evbit4 = evbit2 | (evbit2 >> 2);
    const uint64_t odbit4 = odbit2 | (odbit2 >> 2);

    const uint64_t evbit8 = evbit4 | (evbit4 >> 4);
    const uint64_t odbit8 = odbit4 | (odbit4 >> 4);

    const uint64_t evbit16 = evbit8 | (evbit8 >> 8);
    const uint64_t odbit16 = odbit8 | (odbit8 >> 8);

    return evbit16 | odbit16;
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_cmpeq32 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64i_lo (a) == simd64i_lo (b) ? 0xffffffff : 0,
			  simd64i_hi (a) == simd64i_hi (b) ? 0xffffffff : 0);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_cmpeq64 (simd64i a, simd64i b)
{
    return a == b ? simd64i_dup8 (0xff) : 0;
}

__SIMD_PURE_FUNC simd64i simd64i_cmplt32 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64i_lo (a) < simd64i_lo (b) ? 0xffffffff : 0,
			  simd64i_hi (a) < simd64i_hi (b) ? 0xffffffff : 0);
#endif
}

__SIMD_PURE_FUNC simd64i simd64i_cmple32 (simd64i a, simd64i b)
{
#if __SIMD_NEON
#error
#else
    return simd64i_set32 (simd64i_lo (a) <= simd64i_lo (b) ? 0xffffffff : 0,
			  simd64i_hi (a) <= simd64i_hi (b) ? 0xffffffff : 0);
#endif
}

__SIMD_FORWARD_FUNC simd64f simd64f_set (float v0, float v1);

__SIMD_PURE_FUNC simd64f simd64i_cvtf (simd64i a)
{
#if __SIMD_NEON
#error
#else
    return simd64f_set (simd64i_lo (a), simd64i_hi (a));
#endif
}

__SIMD_CONST_FUNC simd64i simd64i_gather8x8 (const void *base,
					     simd64i a0,
					     simd64i a1,
					     simd64i a2,
					     simd64i a3)
{
    union {
	uint64_t _[4];
	uint32_t a32[8];
    } av = {{ 
	simd64i_get64 (a0),
	simd64i_get64 (a1),
	simd64i_get64 (a2),
	simd64i_get64 (a3),
    }};

    return

    (union {
	uint8_t _[8];
	uint64_t v;
    })
    {{
	((const uint8_t *) base)[av.a32[0]],
	((const uint8_t *) base)[av.a32[1]],
	((const uint8_t *) base)[av.a32[2]],
	((const uint8_t *) base)[av.a32[3]],
	((const uint8_t *) base)[av.a32[4]],
	((const uint8_t *) base)[av.a32[5]],
	((const uint8_t *) base)[av.a32[6]],
	((const uint8_t *) base)[av.a32[7]],
    }}.v;
}

__SIMD_CONST_FUNC simd64i simd64i_gather8a32x2 (const void *base,
						simd64i a)
{
    union {
	uint64_t a64[1];
	uint32_t a32[2];
    } av = {{ simd64i_get64 (a) }};

    return (uint64_t) ((const uint8_t *) base)[av.a32[0]] << 0 |
	   (uint64_t) ((const uint8_t *) base)[av.a32[1]] << 32;
}

#include <stdio.h>
#include <simd.h>

int main (int argc, char **argv)
{
    simd64i a = simd64i_set32 (-1, 0);
    simd64i b = simd64i_set32 (11, 0);

    simd64i c = simd64i_min32s (a, b);

    printf ("%d\n", simd64i_get32 (c, 0));

    printf ("%d\n", simd128i_get8 (simd128i_dup8 (100), 0));

    return 0;
}
